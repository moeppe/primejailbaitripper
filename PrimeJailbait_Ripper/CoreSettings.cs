﻿//-----------------------------------------------------------------------
// <copyright file="CoreSettings.cs" company="RAN COMMUNITY SERVER">
//     (C)2010-2013 Marcel Nolte
// </copyright>
//-----------------------------------------------------------------------

namespace PrimeJailbait_Ripper
{
    using System.Net;

    /// <summary>
    /// Basic program settings
    /// </summary>
    public static class CoreSettings
    {
        /// <summary>
        /// Initializes static members of the <see cref="CoreSettings" /> class.
        /// </summary>
        static CoreSettings()
        {
            MaximumSimultanousDownloads = 20;
            MaximumSimultanousJobs = 1;
            BaseTargetPath = TargetPath = "I:\\_sabrina\\PrimeJB";
            ShowPreview = false;
            PictureTarget = null;
            TextTarget = null;
            Proxy = null;
            //// System.Net.NetworkCredential auth = new System.Net.NetworkCredential("marcel", "squid99752Ubd");
            //// Proxy = new System.Net.WebProxy("maria.noltecomputer.com", 55580);
            //// Proxy.Credentials = auth;
        }

        /// <summary>
        /// Gets the proxy to use
        /// </summary>
        public static WebProxy Proxy { get; private set; }

        /// <summary>
        /// Gets or sets the maximum parallel downloads
        /// </summary>
        public static int MaximumSimultanousDownloads { get; set; }

        /// <summary>
        /// Gets or sets the maximum parallel jobs (currently doesn't work)
        /// </summary>
        public static int MaximumSimultanousJobs { get; set; }

        /// <summary>
        /// Gets or sets the basic download target
        /// </summary>
        public static string BaseTargetPath { get; set; }

        /// <summary>
        /// Gets or sets the download target
        /// </summary>
        public static string TargetPath { get; set; }

        /// <summary>
        /// Gets or sets the left pane which controls progress bars
        /// </summary>
        public static System.Windows.Forms.Control Container { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a preview of downloaded files should be shown or not
        /// </summary>
        public static bool ShowPreview { get; set; }

        /// <summary>
        /// Gets or sets the picture box for preview images
        /// </summary>
        public static System.Windows.Forms.PictureBox PictureTarget { get; set; }

        /// <summary>
        /// Gets or sets the label for status messages
        /// </summary>
        public static System.Windows.Forms.ToolStripStatusLabel TextTarget { get; set; }

        public static string UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; de; rv:1.9.2.17) Gecko/20110420 Firefox/3.6.17";

        public static string Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

        public static string[] AdditionalHeaders = new string[]{
            "Accept-Language: en-us,de-de;q=0.8,de;q=0.5,en;q=0.3"
        };
    }
}
