﻿namespace PrimeJailbait_Ripper
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Windows.Forms;
    using System.Drawing;
    using System.IO;
    using System.Threading;

    using Nolte.IO;

    using Timer = System.Windows.Forms.Timer;

    public partial class Form1 : Form
    {
        public void TopCollectionPageFinished(Job job)
        {
            if (job.Result is string)
            {
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Collections", job.CurrentTag)))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Collections", job.CurrentTag));

                string s = job.Result.Substring(job.Result.IndexOf("<div id='thumbs'>") + 17);
                foreach (
                    string ss in s.Split(new string[] { "<div class='thumb'" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (ss.Trim() != "")
                    {
                        string url = ss.Substring(ss.IndexOf("http://www.primejailbait.com/id/"));
                        url = url.Substring(0, url.IndexOf('\''));
                        string id = url.Substring(0, url.Length - 1);
                        id = id.Substring(id.LastIndexOf("/") + 1);
                        string tags =
                            ss.Substring(ss.IndexOf("<img src='http://www.primejailbait.com/pics/bigthumbs/"));
                        tags = tags.Substring(tags.IndexOf("alt='") + 5);
                        string[] tagss = this.ParseTags(tags);
                        Job newJob = new Job() { FileId = id };
                        foreach (string tag in tagss)
                        {
                            newJob.TargetDirectories.Add(Combine(CoreSettings.BaseTargetPath, "Tags", tag));
                        }
                        newJob.TargetDirectories.Add(Combine(CoreSettings.BaseTargetPath, "Collections", job.CurrentTag));
                        if (Catalog.ContainsKey(id))
                        {
                            string fn = Catalog[id];
                            newJob.TargetFile = Combine(CoreSettings.BaseTargetPath, "All", fn);
                            newJob.Filename = fn;
                            this.ImageFinished(newJob);
                            imagesFound++;
                        }
                        else
                        {
                            newJob.Url = url.Trim();
                            newJob.OnFinished = this.ImagePageFinished;
                            StringJobs.Enqueue(newJob);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Error in job " + job.Url);
            }
        }

        public void TopCollectionsOverviewPageFinished(Job job)
        {
            if (job.Result is string)
            {
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Collections")))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Collections"));
                string s = job.Result.Substring(job.Result.IndexOf("<div id='thumbs'>") + 17);
                foreach (
                    string ss in s.Split(new string[] { "<div class='thumb'" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (ss.Trim() != "")
                    {
                        string url = ss.Substring(ss.IndexOf("<a href='") + 9);
                        url = url.Substring(0, url.IndexOf('\''));
                        string name = ss.Substring(ss.IndexOf("<span>") + 6);
                        name = this.StripTagsCharArray(name.Substring(0, name.IndexOf("</span")));
                        StringJobs.Enqueue(
                            new Job()
                            {
                                Url = url.Trim(),
                                CurrentTag = name.Trim(),
                                OnFinished = this.TopCollectionPageFinished
                            });
                    }
                }
            }
            else
            {
                Console.WriteLine("Error in job " + job.Url);
            }
        }

        public void YoungestPageFinished(Job job)
        {
            if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Youngest", job.CurrentAge.ToString())))
                Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Youngest", job.CurrentAge.ToString()));

            if (job.Result is string)
            {
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Collections")))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Collections"));
                if (job.Result.Contains("href=\"/gallery/static/newest/") && job.CurrentPage < job.MaximumPage)
                {
                    string next_page = job.Result.Substring(job.Result.IndexOf("href=\"/gallery/static/newest/") + 6);
                    next_page = "http://www.primejailbait.com" + next_page.Substring(0, next_page.IndexOf("\""));
                    if (!next_page.EndsWith("nosets/" + job.CurrentAge.ToString() + "/"))
                    {
                        next_page += "nosets/" + job.CurrentAge.ToString() + "/";
                    }
                    StringJobs.Enqueue(
                        new Job()
                        {
                            Url = next_page,
                            CurrentAge = job.CurrentAge,
                            CurrentPage = job.CurrentPage + 1,
                            MaximumPage = job.MaximumPage,
                            OnFinished = this.YoungestPageFinished
                        });
                }
                string s = job.Result.Substring(job.Result.IndexOf("<div id='thumbs'>") + 17);
                foreach (
                    string ss in s.Split(new string[] { "<div class='thumb'" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (ss.Trim() != "" && ss.Contains("http://www.primejailbait.com/id/"))
                    {
                        string url = ss.Substring(ss.IndexOf("http://www.primejailbait.com/id/"));
                        url = url.Substring(0, url.IndexOf('\''));
                        string id = url.Substring(0, url.Length - 1);
                        id = id.Substring(id.LastIndexOf("/") + 1);
                        string tags =
                            ss.Substring(ss.IndexOf("<img src='http://www.primejailbait.com/pics/bigthumbs/"));
                        tags = tags.Substring(tags.IndexOf("alt='") + 5);
                        string[] tagss = this.ParseTags(tags);
                        Job newJob = new Job() { FileId = id };
                        foreach (string tag in tagss)
                        {
                            newJob.TargetDirectories.Add(Combine(CoreSettings.BaseTargetPath, "Tags", tag));
                        }
                        newJob.TargetDirectories.Add(Combine(CoreSettings.BaseTargetPath, "Youngest", job.CurrentAge.ToString()));
                        if (Catalog.ContainsKey(id))
                        {
                            string fn = Catalog[id];
                            newJob.TargetFile = Combine(CoreSettings.BaseTargetPath, "All", fn);
                            newJob.Filename = fn;
                            this.ImageFinished(newJob);
                            imagesFound++;
                        }
                        else
                        {
                            newJob.Url = url.Trim();
                            newJob.OnFinished = this.ImagePageFinished;
                            StringJobs.Enqueue(newJob);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Error in job " + job.Url);
            }
        }

        public void MostFavedPageFinished(Job job)
        {
            string FavGroup = (Math.Floor((job.CurrentPage - 1) / (decimal)10) + 1).ToString();
            if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Most Faved", FavGroup)))
                Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Most Faved", FavGroup));

            if (job.Result is string)
            {
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Collections")))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Collections"));
                if (job.Result.Contains("href=\"/gallery/static/favs/") && job.CurrentPage < job.MaximumPage)
                {
                    string next_page = job.Result.Substring(job.Result.IndexOf("href=\"/gallery/static/favs/") + 6);
                    next_page = "http://www.primejailbait.com" + next_page.Substring(0, next_page.IndexOf("\""));
                    StringJobs.Enqueue(
                        new Job()
                        {
                            Url = next_page,
                            CurrentPage = job.CurrentPage + 1,
                            MaximumPage = job.MaximumPage,
                            OnFinished = this.MostFavedPageFinished
                        });
                }
                string s = job.Result.Substring(job.Result.IndexOf("<div id='thumbs'>") + 17);
                foreach (
                    string ss in s.Split(new string[] { "<div class='thumb'" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (ss.Trim() != "" && ss.Contains("http://www.primejailbait.com/id/"))
                    {
                        string url = ss.Substring(ss.IndexOf("http://www.primejailbait.com/id/"));
                        url = url.Substring(0, url.IndexOf('\''));
                        string id = url.Substring(0, url.Length - 1);
                        id = id.Substring(id.LastIndexOf("/") + 1);
                        string tags =
                            ss.Substring(ss.IndexOf("<img src='http://www.primejailbait.com/pics/bigthumbs/"));
                        tags = tags.Substring(tags.IndexOf("alt='") + 5);
                        string[] tagss = this.ParseTags(tags);
                        Job newJob = new Job() { FileId = id };
                        foreach (string tag in tagss)
                        {
                            newJob.TargetDirectories.Add(Combine(CoreSettings.BaseTargetPath, "Tags", tag));
                        }
                        newJob.TargetDirectories.Add(Combine(CoreSettings.BaseTargetPath, "Most Faved", FavGroup));
                        if (Catalog.ContainsKey(id))
                        {
                            string fn = Catalog[id];
                            newJob.TargetFile = Combine(CoreSettings.BaseTargetPath, "All", fn);
                            newJob.Filename = fn;
                            this.ImageFinished(newJob);
                            imagesFound++;
                        }
                        else
                        {
                            newJob.Url = url.Trim();
                            newJob.OnFinished = this.ImagePageFinished;
                            StringJobs.Enqueue(newJob);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Error in job " + job.Url);
            }
        }

        public void MostViewedPageFinished(Job job)
        {
            string FavGroup = (Math.Floor((job.CurrentPage - 1) / (decimal)10) + 1).ToString();
            if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Most Viewed", FavGroup)))
                Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Most Viewed", FavGroup));

            if (job.Result is string)
            {
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Collections")))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Collections"));
                if (job.Result.Contains("href=\"/gallery/static/favs/") && job.CurrentPage < job.MaximumPage)
                {
                    string next_page = job.Result.Substring(job.Result.IndexOf("href=\"/gallery/static/views/") + 6);
                    next_page = "http://www.primejailbait.com" + next_page.Substring(0, next_page.IndexOf("\""));
                    StringJobs.Enqueue(
                        new Job()
                        {
                            Url = next_page,
                            CurrentPage = job.CurrentPage + 1,
                            MaximumPage = job.MaximumPage,
                            OnFinished = this.MostViewedPageFinished
                        });
                }
                string s = job.Result.Substring(job.Result.IndexOf("<div id='thumbs'>") + 17);
                foreach (
                    string ss in s.Split(new string[] { "<div class='thumb'" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (ss.Trim() != "" && ss.Contains("http://www.primejailbait.com/id/"))
                    {
                        string url = ss.Substring(ss.IndexOf("http://www.primejailbait.com/id/"));
                        url = url.Substring(0, url.IndexOf('\''));
                        string id = url.Substring(0, url.Length - 1);
                        id = id.Substring(id.LastIndexOf("/") + 1);
                        string tags =
                            ss.Substring(ss.IndexOf("<img src='http://www.primejailbait.com/pics/bigthumbs/"));
                        tags = tags.Substring(tags.IndexOf("alt='") + 5);
                        string[] tagss = this.ParseTags(tags);
                        Job newJob = new Job() { FileId = id };
                        foreach (string tag in tagss)
                        {
                            newJob.TargetDirectories.Add(Combine(CoreSettings.BaseTargetPath, "Tags", tag));
                        }
                        newJob.TargetDirectories.Add(Combine(CoreSettings.BaseTargetPath, "Most Viewed", FavGroup));
                        if (Catalog.ContainsKey(id))
                        {
                            string fn = Catalog[id];
                            newJob.TargetFile = Combine(CoreSettings.BaseTargetPath, "All", fn);
                            newJob.Filename = fn;
                            this.ImageFinished(newJob);
                            imagesFound++;
                        }
                        else
                        {
                            newJob.Url = url.Trim();
                            newJob.OnFinished = this.ImagePageFinished;
                            StringJobs.Enqueue(newJob);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Error in job " + job.Url);
            }
        }

        public void StartDownloads()
        {
            button1.Text = "Pause";
            if (firstRun)
            {
                Properties.Settings.Default.Age13 = checkBox1.Checked;
                Properties.Settings.Default.Age14 = checkBox2.Checked;
                Properties.Settings.Default.Age15 = checkBox3.Checked;
                Properties.Settings.Default.Age16 = checkBox4.Checked;
                Properties.Settings.Default.Age17 = checkBox5.Checked;
                Properties.Settings.Default.Age18 = checkBox6.Checked;
                Properties.Settings.Default.MostViewd = checkBox9.Checked;
                Properties.Settings.Default.MostViewedMaxDepth = (int)numericUpDown2.Value;
                Properties.Settings.Default.AgeMaxDepth = (int)numericUpDown3.Value;
                Properties.Settings.Default.Collections = checkBox7.Checked;
                Properties.Settings.Default.MostFaved = checkBox8.Checked;
                Properties.Settings.Default.MostFavedMaxDepth = (int)numericUpDown1.Value;
                CoreSettings.TargetPath = CoreSettings.BaseTargetPath = Properties.Settings.Default.TargetDir = txtTargetDir.Text;
                Properties.Settings.Default.Save();
                button2.Enabled = numericUpDown1.Enabled = false;
                /*  checkBox1.Enabled =
                      checkBox2.Enabled =
                          checkBox3.Enabled =
                              checkBox4.Enabled = checkBox5.Enabled = checkBox6.Enabled = checkBox7.Enabled = false;*/
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Collections")))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Collections"));
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "All")))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "All"));
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Tags")))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Tags"));
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Youngest")))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Youngest"));
                if (!Directory.Exists(Combine(CoreSettings.BaseTargetPath, "Most Faved")))
                    Directory.CreateDirectory(Combine(CoreSettings.BaseTargetPath, "Most Faved"));
                this.LoadCatalog();
                FileJobs.Clear();
                StringJobs.Clear();
                if (checkBox1.Checked)
                    StringJobs.Enqueue(
                        new Job()
                        {
                            Url =
                                "http://www.primejailbait.com/gallery/static/newest/1/desc/1500000/nosets/13/",
                                CurrentPage = 1,
                                MaximumPage = (int)numericUpDown3.Value,
                                CurrentAge = 13,
                            OnFinished = this.YoungestPageFinished
                        });
                if (checkBox2.Checked)
                    StringJobs.Enqueue(
                        new Job()
                        {
                            Url =
                                "http://www.primejailbait.com/gallery/static/newest/1/desc/1500000/nosets/14/",
                            CurrentPage = 1,
                            MaximumPage = (int)numericUpDown3.Value,
                            CurrentAge = 14,
                            OnFinished = this.YoungestPageFinished
                        });
                if (checkBox3.Checked)
                    StringJobs.Enqueue(
                        new Job()
                        {
                            Url =
                                "http://www.primejailbait.com/gallery/static/newest/1/desc/1500000/nosets/15/",
                            CurrentPage = 1,
                            MaximumPage = (int)numericUpDown3.Value,
                            CurrentAge = 15,
                            OnFinished = this.YoungestPageFinished
                        });
                if (checkBox4.Checked)
                    StringJobs.Enqueue(
                        new Job()
                        {
                            Url =
                                "http://www.primejailbait.com/gallery/static/newest/1/desc/1500000/nosets/16/",
                            CurrentPage = 1,
                            MaximumPage = (int)numericUpDown3.Value,
                            CurrentAge = 16,
                            OnFinished = this.YoungestPageFinished
                        });
                if (checkBox5.Checked)
                    StringJobs.Enqueue(
                        new Job()
                        {
                            Url =
                                "http://www.primejailbait.com/gallery/static/newest/1/desc/1500000/nosets/17/",
                            CurrentPage = 1,
                            MaximumPage = (int)numericUpDown3.Value,
                            CurrentAge = 17,
                            OnFinished = this.YoungestPageFinished
                        });
                if (checkBox6.Checked)
                    StringJobs.Enqueue(
                        new Job()
                        {
                            Url =
                                "http://www.primejailbait.com/gallery/static/newest/1/desc/1500000/nosets/18/",
                            CurrentPage = 1,
                            MaximumPage = (int)numericUpDown3.Value,
                            CurrentAge = 18,
                            OnFinished = this.YoungestPageFinished
                        });
                if (checkBox7.Checked)
                    StringJobs.Enqueue(
                    new Job()
                    {
                        Url = "http://www.primejailbait.com/collections/",
                        OnFinished = this.TopCollectionsOverviewPageFinished
                    });
                if (checkBox8.Checked)
                    StringJobs.Enqueue(
                    new Job()
                    {
                        Url = "http://www.primejailbait.com/gallery/static/favs/1/desc/15000/",
                        CurrentPage = 1,
                        MaximumPage = (int)numericUpDown1.Value,
                        OnFinished = this.MostFavedPageFinished
                    });
                if (checkBox9.Checked)
                    StringJobs.Enqueue(
                    new Job()
                    {
                        Url = "http://www.primejailbait.com/gallery/static/views/1/desc/150000000/",
                        CurrentPage = 1,
                        MaximumPage = (int)numericUpDown2.Value,
                        OnFinished = this.MostViewedPageFinished
                    });

                firstRun = false;
            }
            saveTimer.Start();
            timer.Start();
        }

        public void LoadSettings()
        {
            checkBox1.Checked = Properties.Settings.Default.Age13;
            checkBox2.Checked = Properties.Settings.Default.Age14;
            checkBox3.Checked = Properties.Settings.Default.Age15;
            checkBox4.Checked = Properties.Settings.Default.Age16;
            checkBox5.Checked = Properties.Settings.Default.Age17;
            checkBox6.Checked = Properties.Settings.Default.Age18;
            checkBox7.Checked = Properties.Settings.Default.Collections;
            checkBox8.Checked = Properties.Settings.Default.MostFaved;
            numericUpDown1.Value = (decimal)Properties.Settings.Default.MostFavedMaxDepth;
            checkBox9.Checked = Properties.Settings.Default.MostViewd;
            numericUpDown2.Value = (decimal)Properties.Settings.Default.MostViewedMaxDepth;
            numericUpDown3.Value = (decimal)Properties.Settings.Default.AgeMaxDepth;
            txtTargetDir.Text = Properties.Settings.Default.TargetDir;
        }

        public Queue<Job> FileJobs = new Queue<Job>();
        public Queue<Job> StringJobs = new Queue<Job>(); 
        public Timer timer = new Timer();
        public Timer saveTimer = new Timer();

        protected bool firstRun = true;

        public Job[] slots = new Job[5];
        public ProgressBar[] progressBars = new ProgressBar[5];
        public LinkLabel[] linkLabels = new LinkLabel[5];

        public Dictionary<string, string> Catalog = new Dictionary<string, string>();
        public Dictionary<string, string> NewCatalog = new Dictionary<string, string>();

        public string CurrentCatalogString = "";

        public Form1()
        {
            InitializeComponent();
            timer.Interval = 100;
            timer.Tick += CheckDownloads;
            saveTimer.Interval = 300000;
            saveTimer.Tick += SaveCatalog;
            progressBars[0] = progressBar1;
            progressBars[1] = progressBar2;
            progressBars[2] = progressBar3;
            progressBars[3] = progressBar4;
            progressBars[4] = progressBar5;
            linkLabels[0] = linkLabel1;
            linkLabel1.LinkClicked += (o, e) =>
            {
                System.Diagnostics.Process.Start(linkLabel1.Text);
            };
            linkLabels[1] = linkLabel2;
            linkLabel2.LinkClicked += (o, e) =>
            {
                System.Diagnostics.Process.Start(linkLabel2.Text);
            };
            linkLabels[2] = linkLabel3;
            linkLabel3.LinkClicked += (o, e) =>
            {
                System.Diagnostics.Process.Start(linkLabel3.Text);
            };
            linkLabels[3] = linkLabel4;
            linkLabel4.LinkClicked += (o, e) =>
            {
                System.Diagnostics.Process.Start(linkLabel4.Text);
            };
            linkLabels[4] = linkLabel5;
            linkLabel5.LinkClicked += (o, e) =>
            {
                System.Diagnostics.Process.Start(linkLabel5.Text);
            };
            if (!Properties.Settings.Default.LastWindowSize.IsEmpty)
            {
                this.Size = Properties.Settings.Default.LastWindowSize;
            }
            if (Properties.Settings.Default.LastWindowState != FormWindowState.Minimized)
            {
                this.WindowState = Properties.Settings.Default.LastWindowState;
            }
            this.LoadSettings();
        }

        public void LoadCatalog()
        {
            if (AlternateDataStreams.Exists(CoreSettings.BaseTargetPath, "Catalog.dat"))
            {
                CurrentCatalogString = AlternateDataStreams.Read(CoreSettings.BaseTargetPath, "Catalog.dat");
                foreach (string line in
                    CurrentCatalogString.Split('\n'))
                {
                    if (!Catalog.ContainsKey(line.Split('=')[0])) Catalog.Add(line.Split('=')[0], line.Split('=')[1]);
                }
            }

        }

        private void SaveCatalog(object sender = null, EventArgs eventArgs = null)
        {
            bool whileRunning = timer.Enabled;
            if (whileRunning)
            {
                timer.Stop();
                bool anyRunning = false;
                do
                {
                    anyRunning = false;
                    for (int i = 0; i < 5; i++)
                    {
                        if (this.slots[i] is Job && !this.slots[i].Success)
                        {
                            anyRunning = true;
                        }
                    }
                    Application.DoEvents();
                    Thread.Sleep(100);
                }
                while (anyRunning);                
            }
            string result = CurrentCatalogString;
            foreach (KeyValuePair<string, string> line in NewCatalog)
            {
                result += String.Format("{2}{0}={1}", line.Key, line.Value, result.Length == 0 ? "" : "\n");
            }
            NewCatalog.Clear();
            AlternateDataStreams.Write(result, CoreSettings.BaseTargetPath, "Catalog.dat");
            CurrentCatalogString = result;
            if (whileRunning)
                timer.Start();
        }

        public void AddToCatalog(string id, string filename)
        {
            if (!Catalog.ContainsKey(id))
            {
                Catalog.Add(id, filename);
                if (!NewCatalog.ContainsKey(id))
                {
                    NewCatalog.Add(id, filename);
                }
            }
        }

        protected string[] specialdirs = new string[]{"com1", "com2", "com3", "com4", "com5", "com6", "com7", "com8", "con", "nul", "lpt1", "lpt2", "lpt3"};

        public string EscapeSpecialDir(string dir)
        {
            foreach (string specialdir in specialdirs)
            {
                if (specialdir == dir) return "_" + dir;
            }
            return dir;
        }

        public string Combine(string str1, string str2)
        {
            return String.Format("{0}\\{1}", str1.Trim('\\'), EscapeSpecialDir(str2).Trim('\\'));
        }

        public string Combine(string str1, string str2, string str3)
        {
            return String.Format("{0}\\{1}\\{2}", str1.Trim('\\'), EscapeSpecialDir(str2).Trim('\\'), EscapeSpecialDir(str3).Trim('\\'));
        }

        public string Combine(string str1, string str2, string str3, string str4)
        {
            return String.Format("{0}\\{1}\\{2}\\{3}", str1.Trim('\\'), EscapeSpecialDir(str2).Trim('\\'), EscapeSpecialDir(str3).Trim('\\'), EscapeSpecialDir(str4).Trim('\\'));
        }

        private void CheckDownloads(object sender, EventArgs eventArgs)
        {
            this.Text = "PrimeJailbait Ripper (" + imagesFound.ToString() + "/" + Catalog.Count.ToString() + ")";
            bool anyStartet = false;
            for (int i = 0; i < slots.Length; i++)
            {
                if (!(slots[i] is Job) || slots[i].Success)
                {
                    if (FileJobs.Count > 0)
                    {
                        Job j = FileJobs.Dequeue();
                        slots[i] = j;
                        progressBars[i].Value = 0;
                        ProgressBar pg = progressBars[i];
                        linkLabels[i].Text = j.Url;
                        WebClient e = new WebClient();
                        e.Headers["UserAgent"] = CoreSettings.UserAgent;
                        e.Headers["Accept"] = CoreSettings.Accept;
                        e.Headers["Accept-Language"] = "en-us,de-de;q=0.8,de;q=0.5,en;q=0.3";
                        e.Headers["Referer"] = "http://www.primejailbait.com/id/1316948/";
                        e.DownloadFileCompleted += (o, args) =>
                        {
                            j.Success = true;
                            j.OnFinished(j);
                        };
                        e.DownloadProgressChanged += (o, args) =>
                        {
                            pg.Value = args.ProgressPercentage;
                            Application.DoEvents();
                        };
                        e.DownloadFileAsync(new Uri(j.Url), j.TargetFile);
                        Application.DoEvents();
                        anyStartet = true;
                    } else if (StringJobs.Count > 0)
                    {
                        Job j = StringJobs.Dequeue();
                        ProgressBar pg = progressBars[i];
                        slots[i] = j;
                        progressBars[i].Value = 0;
                        linkLabels[i].Text = j.Url;
                        WebClient e = new WebClient();
                        e.Headers["UserAgent"] = CoreSettings.UserAgent;
                        e.Headers["Accept"] = CoreSettings.Accept;
                        e.DownloadStringCompleted += (o, args) =>
                        {
                            j.Result = args.Result;
                            j.Success = true;
                            j.OnFinished(j);
                        };
                        e.DownloadProgressChanged += (o, args) =>
                        {
                            pg.Value = args.ProgressPercentage;
                            //Application.DoEvents();
                            pg.Refresh();
                        };
                        e.DownloadStringAsync(new Uri(j.Url));
                        //Application.DoEvents();
                        anyStartet = true;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            if (!anyStartet)
            {
                bool anyRunning = false;
                for (int i = 0; i < 5; i++)
                {
                    if (this.slots[i] is Job && this.slots[i].Success)
                    {
                        anyRunning = true;
                    }
                }
                if (!anyRunning && false)
                {
                    timer.Stop();
                    saveTimer.Stop();
                    this.Close();
                }
            }
        }

        public void SuspendDownloads()
        {
            button1.Text = "Fortsetzen";
            saveTimer.Stop();
            timer.Stop();            
        }

        public string[] ParseTags(string tags)
        {
            return
                tags.Substring(0, tags.IndexOf('\'')).Replace(" jailbait pic", "")
                    .Trim()
                    .Replace(":", "ː")
                    .Replace("?", "‽")
                    .Replace("*", "★")
                    .Replace("\"", "''")
                    .Replace("\\", "/")
                    .Replace("..", ". .")
                    .Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
        }

        private void Button1Click(object sender, EventArgs e)
        {
            if (!timer.Enabled)
            {
                this.StartDownloads();
            }
            else
            {
                this.SuspendDownloads();
            }
        }

        public void ImageFinished(Job job)
        {
            bool anyNew = false;
            foreach (string dir in job.TargetDirectories)
            {
                if (!Directory.Exists(dir)) 
                    Directory.CreateDirectory(dir);
                if (!File.Exists(dir + "\\" + job.Filename))
                {
                    Hardlink.Create(
                        dir + "\\" + job.Filename,
                        job.TargetFile);
                    anyNew = !dir.Contains("\\Youngest\\"); // RÜCKGÄNGIG MACHEN true;                    
                }
            }
            if (anyNew)
            {
                try
                {
                    pictureBox1.Image = Image.FromFile(job.TargetFile);
                }
                catch
                {
                }
            }
            if (!AlternateDataStreams.Exists(job.TargetFile, "primejailbait"))
            {
                AlternateDataStreams.Write(job.FileId, job.TargetFile, "primejailbait");
            }
            this.AddToCatalog(job.FileId, job.Filename);
        }

        public string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        private void SaveOnClose(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.LastWindowState = WindowState;
            if (WindowState != FormWindowState.Minimized)
                Properties.Settings.Default.LastWindowSize = Size;
            Properties.Settings.Default.Save();
            saveTimer.Stop();
            timer.Stop();
            bool anyRunning = false;
            do
            {
                anyRunning = false;
                for (int i = 0; i < 5; i++)
                {
                    if (this.slots[i] is Job && !this.slots[i].Success)
                    {
                        anyRunning = true;
                    }
                }
                Application.DoEvents();
                Thread.Sleep(100);
            }
            while (anyRunning);
            this.SaveCatalog();
        }

        public void ImagePageFinished(Job job)
        {
            if (job.Result is string)
            {
                string s = job.Result.Substring(job.Result.IndexOf("<div id=\"bigwall\""));
                s = s.Substring(s.IndexOf("src='") + 5);
                s = s.Substring(0, s.IndexOf("'"));
                string fn = s.Substring(s.LastIndexOf('/') + 1);
                Job newJob = new Job()
                {
                    Url = s.Trim(),
                    TargetFile = Combine(CoreSettings.BaseTargetPath, "All", fn),
                    OnFinished = this.ImageFinished,
                    TargetDirectories = job.TargetDirectories,
                    FileId = job.FileId,
                    Filename = fn
                };
                if (!File.Exists(Combine(CoreSettings.BaseTargetPath, "All", fn)))
                {
                    FileJobs.Enqueue(newJob);
                }
                else
                {
                    this.ImageFinished(newJob);
                }
                imagesFound++;
            }
            else
            {
                Console.WriteLine("Error in job " + job.Url);
            }
        }

        protected int imagesFound = 0;

        private void BrowseForTargetDir(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                fbd.SelectedPath = this.txtTargetDir.Text;
                if (fbd.ShowDialog(this) == DialogResult.OK)
                {
                    this.txtTargetDir.Text = fbd.SelectedPath;
                }
            }
        }
    }

    internal delegate void JobFinishedEventHandler(Job job);
    public class Job
    {
        public string Url { get; set; }
        internal JobFinishedEventHandler OnFinished { get; set; }
        public string Result { get; set; }
        public string TargetFile { get; set; }
        public bool Success { get; set; }
        public List<string> TargetDirectories = new List<string>();
        public string FileId { get; set; }
        public string Filename { get; set; }
        public int CurrentPage { get; set; }
        public int MaximumPage { get; set; }
        public string CurrentTag { get; set; }
        public int CurrentAge { get; set; }

        public Job()
        {
            Success = false;
        }
    }

    public enum JobType
    {
        String,
        File
    }
}
