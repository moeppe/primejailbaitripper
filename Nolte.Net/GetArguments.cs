﻿//-----------------------------------------------------------------------
// <copyright file="PostArguments.cs" company="RAN COMMUNITY SERVER">
//     (C)2010-2013 Marcel Nolte
// </copyright>
//-----------------------------------------------------------------------

namespace Nolte.Net
{
    using System.Collections.Generic;
    using System.Text;
    using System.Web;

    /// <summary>
    /// Represents a collection of uri parameters to submit on a request
    /// </summary>
    public class GetArguments
    {
        /// <summary>
        /// A collection of the data to submit
        /// </summary>
        private readonly Dictionary<string, string> getData = new Dictionary<string, string>();

        /// <summary>
        /// Gets the encoded content 
        /// </summary>
        public byte[] Content
        {
            get
            {
                string content = string.Empty;
                foreach (var data in this.getData)
                {
                    content += (string.IsNullOrWhiteSpace(content) ? string.Empty : "&") + HttpUtility.UrlEncode(data.Key) + "=" + HttpUtility.UrlEncode(data.Value);
                }

                return Encoding.ASCII.GetBytes(content);
            }
        }

        /// <summary>
        /// Gets the property's value identified by it's name
        /// </summary>
        /// <param name="name">The name of the property to search for</param>
        /// <returns>The property's value</returns>
        public string this[string name]
        {
            get
            {
                return this.getData.ContainsKey(name) ? this.getData[name] : string.Empty;
            }
        }

        /// <summary>
        /// Add a key/value pair to the data collection
        /// </summary>
        /// <param name="name">The property's name</param>
        /// <param name="value">The property's value</param>
        public void Add(string name, string value)
        {
            if (this.getData.ContainsKey(name))
            {
                this.getData[name] = value;
            }
            else
            {
                this.getData.Add(name, value);
            }
        }

        /// <summary>
        /// Removes the given property
        /// </summary>
        /// <param name="name">The name of the property to remove</param>
        public void Remove(string name)
        {
            if (this.getData.ContainsKey(name))
            {
                this.getData.Remove(name);
            }
        }

        /// <summary>
        /// Checks if the given property is set
        /// </summary>
        /// <param name="name">The name of the property to search for</param>
        /// <returns>True, if the property exists</returns>
        public bool Contains(string name)
        {
            return this.getData.ContainsKey(name);
        }

        public int Count
        {
            get
            {
                return getData.Count;
            }
        }
    }
}
